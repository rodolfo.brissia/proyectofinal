// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqg_1g6bS-5AxE1Ty681-ZDhe2XKr2ZUc",
  authDomain: "proyectowebfinal-4c252.firebaseapp.com",
  databaseURL: "https://proyectowebfinal-4c252-default-rtdb.firebaseio.com",
  projectId: "proyectowebfinal-4c252",
  storageBucket: "proyectowebfinal-4c252.appspot.com",
  messagingSenderId: "680301747962",
  appId: "1:680301747962:web:ac70bc58e34ddc0dd3f343"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas Variables global
var numSerie = 0;
var marca = "";
var modelo = "";
var descripcion = "";
var urlImag = "";

// listar productos
Listarproductos(marca)
function Listarproductos() {
    const titulo= document.getElementById("titulo");
    const  section = document.getElementById('secArticulos')
    titulo.innerHTML= marca; 
    
    
  const dbRef = refS(db, 'Automoviles');
  const tabla = document.getElementById('tablaProductos');
 
  onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
          const childKey = childSnapshot.key;

          const data = childSnapshot.val();
         

        

          section.innerHTML+= "<div class ='card'> " +
                     "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200'> "
                     + "<h1 id='marca'  class='title'>" + data.marca+ ":" + data.modelo +"</h1> "
                     + "<p  id='descripcion' class ='anta-regurar'>" + data.descripcion 
                     +  "</p> <button> Mas Informacion </button> </div>"  ;           


      });
  }, { onlyOnce: true });
}

