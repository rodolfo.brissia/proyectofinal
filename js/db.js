// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAcd4nmM-bSh8l3mfxZO17Kh1gknIbZANI",
    authDomain: "proyectowebmundo-81107.firebaseapp.com",
    databaseURL: "https://proyectowebmundo-81107-default-rtdb.firebaseio.com",
    projectId: "proyectowebmundo-81107",
    storageBucket: "proyectowebmundo-81107.appspot.com",
    messagingSenderId: "600676758823",
    appId: "1:600676758823:web:1dcb27e878bb55d3f2a502"
};




// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

//declarar unas Variables global
var numCodigo = 0;
var categoria = "";
var nombreL = "";
var precio = 0;
var urlImag = "";

// Listar Productos
Listarproductos();

function Listarproductos() {
    const section = document.getElementById("sectProductos");

    const dbref = refS(db, 'Productos');


    onValue(dbref, (snapshot) => {
        const productosPorCategoria = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const categoria = data.categoria;

            if (!productosPorCategoria[categoria]) {
                productosPorCategoria[categoria] = [];
            }
            productosPorCategoria[categoria].push(data);
        });


        section.innerHTML = '';

        for (const [categoria, productos] of Object.entries(productosPorCategoria)) {
            section.innerHTML += `<hr><h2>${categoria}</h2>`;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card'>
                    <img id='urlImag' src='${producto.urlImag}' alt='' width='100'>
                    <p id='nombreL' class='anta-regurar'>${producto.nombreL}</p>
                    <p id='precio'>${producto.precio}</p>
                    <button>Agregar al carrito</button>
                </div>`;
            });
        }

    }, {onlyOnce : true});

}